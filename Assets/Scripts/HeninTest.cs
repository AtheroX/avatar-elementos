﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeninTest : MonoBehaviour {

	#region Variables
	public Image Q, W, E, R;
	public Text BGTxtHenin;
	public Image BG;

	public string StrHenin;
	#endregion

	#region UnityMethods
	void Update() {
		GetKeyInputs();
		TestForSpace();
	}

	public void GetKeyInputs() {
		if (Input.GetKeyDown(KeyCode.Q)) {
			StrHenin += "q";
			BGTxtHenin.text = StrHenin.ToUpper();

		}
		else if (Input.GetKeyDown(KeyCode.W)) {
			StrHenin += "w";
			BGTxtHenin.text = StrHenin.ToUpper();

		}
		else if (Input.GetKeyDown(KeyCode.E)) {
			StrHenin += "e";
			BGTxtHenin.text = StrHenin.ToUpper();

		}
		else if (Input.GetKeyDown(KeyCode.R)) {
			StrHenin += "r";
			BGTxtHenin.text = StrHenin.ToUpper();

		}
		else if (Input.GetKeyDown(KeyCode.C)) {
			StrHenin = "";
			BGTxtHenin.text = StrHenin.ToUpper();

		}
		
	}
	public void TestForSpace() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			if (StrHenin != "") {
				switch (StrHenin) {

					// Henin
					case "q":
						BGTxtHenin.text = "Fire";
						break;
					case "w":
						BGTxtHenin.text = "Water";
						break;
					case "e":
						BGTxtHenin.text = "Stone";
						break;
					case "r":
						BGTxtHenin.text = "Air";
						break;

					// Wiha Fire

					case "qq":
						BGTxtHenin.text = "Fire + Fire";
						break;

					case "qw":
						BGTxtHenin.text = "Fire + Water";
						break;

					case "qe":
						BGTxtHenin.text = "Fire + Stone";
						break;

					case "qr":
						BGTxtHenin.text = "Fire + Air";
						break;

					// Wiha Water

					case "wq":
						BGTxtHenin.text = "Water + Fire";
						break;

					case "ww":
						BGTxtHenin.text = "Water + Water";
						break;

					case "we":
						BGTxtHenin.text = "Water + Stone";
						break;

					case "wr":
						BGTxtHenin.text = "Water + Air";
						break;

					// Wiha Stone

					case "eq":
						BGTxtHenin.text = "Stone + Fire";
						break;

					case "ew":
						BGTxtHenin.text = "Stone + Water";
						break;

					case "ee":
						BGTxtHenin.text = "Stone + Stone";
						break;

					case "er":
						BGTxtHenin.text = "Stone + Air";
						break;

					// Wiha Air

					case "rq":
						BGTxtHenin.text = "Air + Fire";
						break;

					case "rw":
						BGTxtHenin.text = "Air + Water";
						break;

					case "re":
						BGTxtHenin.text = "Air + Stone";
						break;

					case "rr":
						BGTxtHenin.text = "Air + Air";
						break;


					default:
						BGTxtHenin.text = "Unknown";
						break;
				}
			}else {
				BGTxtHenin.text = StrHenin;
			}
			Color BGColor = Color.black;
			foreach (char x in StrHenin) {
				if (x == 'q') {
					BGColor += new Color((255f/StrHenin.Length)*2 / 255f, 0f / 255f, 0f / 255f);
				}else if (x == 'w') {
					BGColor += new Color(0f / 255f, 0f / 255f, (255f / StrHenin.Length)*2 / 255f);
				}else if (x == 'e') {
					BGColor += new Color(0f / 255f, (255f / StrHenin.Length)*2 / 255f, 0f / 255f);
				}else if (x == 'r') {
					BGColor += new Color((255f / StrHenin.Length) / 255f, (255f / StrHenin.Length) / 255f, 0f / 255f);
				}
			}
			Debug.Log(BGColor);
			BGColor -= BGColor /7f; //7 para que no sea un rojo 255,0,0;
			Debug.Log(BGColor);

			BG.color = BGColor;
			StrHenin = "";
		}
	}
	#endregion
}